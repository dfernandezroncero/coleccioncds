class CD:
    def __init__(self,nombre,fecha):
        self.__nombre = nombre
        self.__fecha = fecha
        self.__listaGeneros = []

    def setNombre(self,nombre):
        self.__nombre = nombre

    def getNombre(self):
        return self.__nombre

    def setFecha(self,fecha):
        self.__fecha = fecha

    def getFecha(self):
        return self.__fecha

    def setDiscografica(self, discografica):
        self.__discografica = discografica

    def getDiscografica(self):
        return self.__discografica

    def setIdioma(self, idioma):
        self.__idioma = idioma

    def getIdioma(self):
        return self.__idioma

    def setComprador(self, comprador):
        self.__comprador = comprador

    def getComprador(self):
        return self.__comprador

    def setListaGeneros(self, nuevaLista):
        self.__listaGeneros = nuevaLista

    def getListaGeneros(self):
        return self.__listaGeneros