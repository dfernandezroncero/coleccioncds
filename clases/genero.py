class Genero:

    def __init__(self,nombre, id_nuevo=None):
        self.__nombre = nombre
        self.setId(id_nuevo)

    def setNombre(self,nombre):
        self.__nombre = nombre

    def getNombre(self):
        return self.__nombre

    def setId(self,id_nuevo):
        self.__id = id_nuevo

    def getId(self):
        return self.__id

    def __str__(self):
        return self.__nombre