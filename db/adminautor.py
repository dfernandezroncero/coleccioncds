#IMPORTAR ARCHIVO DE OTRA CARPETA

import mysql.connector
import clases.autor

class AdminAutor:

    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,autor):
        query = ("INSERT INTO autores VALUES (NULL, '%s')" % (autor.getNombre()))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,autor,id_autor):
        query = ("UPDATE autores SET nombre = '%s' WHERE id_autor = %i" % (autor.getNombre(),id_autor))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_autor):
        query = "DELETE FROM autores WHERE id_autor = %i" % (id_autor)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM autores")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_autor):
        query = ("SELECT nombre FROM autores WHERE id_autor = %i" % (id_autor))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()
