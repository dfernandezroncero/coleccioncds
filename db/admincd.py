import mysql.connector
import clases.discografica
import clases.cd

class AdminCD:
    
    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,cd):
        # query = ("INSERT INTO cds VALUES (NULL, '%s', %i, %i, '%s', %i)" % (cd.getNombre(),cd.getIdioma().getId(), cd.getComprador().getId(), cd.getFecha(), cd.getDiscografica().getId()))
        query = ("INSERT INTO cds (nombre, anoAdquirido, id_discografica) VALUES ('%s', '%s', %i)" % (cd.getNombre(), cd.getFecha(), int(cd.getDiscografica().getId())))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,cd,id_cd):
        #query = ("UPDATE cds SET nombre = '%s', id_idioma = %i, id_comprador = %i, anoAdquirido = '%s', id_discografica = %i WHERE id_cd = %i" % (cd.getNombre(),cd.getIdioma().getId(), cd.getComprador().getId(), cd.getFecha(), cd.getDiscografica().getId(),id_cd))
        query = ("UPDATE cds SET nombre = '%s', anoAdquirido = '%s', id_discografica = %i WHERE id_cd = %i" % (cd.getNombre(), cd.getFecha(), cd.getDiscografica().getId(),id_cd))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_cd):
        query = "DELETE FROM cds WHERE id_cd = %i" % (id_cd)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM cds")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_cd):
        query = ("SELECT * FROM cds WHERE id_cd = %i" % (id_cd))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()