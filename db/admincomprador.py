#IMPORTAR ARCHIVO DE OTRA CARPETA

import mysql.connector
import clases.comprador

class AdminComprador:

    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,comprador):
        query = ("INSERT INTO compradores VALUES (NULL, '%s')" % (comprador.getNombre()))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,comprador,id_comprador):
        query = ("UPDATE compradores SET nombre = '%s' WHERE id_comprador = %i" % (comprador.getNombre(),id_comprador))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_comprador):
        query = "DELETE FROM compradores WHERE id_comprador = %i" % (id_comprador)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM compradores")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_comprador):
        query = ("SELECT nombre FROM compradores WHERE id_comprador = %i" % (id_comprador))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()
