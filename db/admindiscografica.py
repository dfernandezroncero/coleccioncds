#IMPORTAR ARCHIVO DE OTRA CARPETA

import mysql.connector
import clases.discografica

class AdminDiscografica:

    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,discografica):
        query = ("INSERT INTO discograficas VALUES (NULL, '%s')" % (discografica.getNombre()))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,discografica,id_discografica):
        query = ("UPDATE discograficas SET nombre = '%s' WHERE id_discografica = %i" % (discografica.getNombre(),id_discografica))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_discografica):
        query = "DELETE FROM discograficas WHERE id_discografica = %i" % (id_discografica)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM discograficas")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_discografica):
        query = ("SELECT nombre FROM discograficas WHERE id_discografica = %i" % (id_discografica))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()