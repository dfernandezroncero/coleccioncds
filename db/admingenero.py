#IMPORTAR ARCHIVO DE OTRA CARPETA

import mysql.connector
import clases.genero

class AdminGenero:

    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,genero):
        query = ("INSERT INTO generos VALUES (NULL, '%s')" % (genero.getNombre()))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,genero,id_genero):
        query = ("UPDATE generos SET nombre = '%s' WHERE id_generos = %i" % (genero.getNombre(),id_genero))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_genero):
        query = "DELETE FROM generos WHERE id_generos = %i" % (id_genero)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM generos")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_genero):
        query = ("SELECT nombre FROM generos WHERE id_generos = %i" % (id_genero))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()