#IMPORTAR ARCHIVO DE OTRA CARPETA

import mysql.connector
import clases.idioma

class AdminIdioma:

    def __init__(self,n_user,n_password,n_host,n_database):
        self.__cnx = mysql.connector.connect(user=n_user, password = n_password, host = n_host, database = n_database)
        self.__cursor = self.__cnx.cursor()

    def crear(self,idioma):
        query = ("INSERT INTO idiomas VALUES (NULL, '%s')" % (idioma.getNombre()))
        self.__cursor.execute(query)
        self.__cnx.commit()
        
    def actualizar(self,idioma,id_idioma):
        query = ("UPDATE idiomas SET nombre = '%s' WHERE id_idioma = %i" % (idioma.getNombre(),id_idioma))
        self.__cursor.execute(query)
        self.__cnx.commit()

    def borrar(self,id_idioma):
        query = "DELETE FROM idiomas WHERE id_idioma = %i" % (id_idioma)
        self.__cursor.execute(query)
        self.__cnx.commit()

    def getALL(self):
        query = ("SELECT * FROM idiomas")
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def getById(self,id_idioma):
        query = ("SELECT nombre FROM idiomas WHERE id_idioma = %i" % (id_idioma))
        self.__cursor.execute(query)
        return self.__cursor.fetchall()

    def closeConnection(self):
        self.__cursor.close()
        self.__cnx.close()