CREATE DATABASE  IF NOT EXISTS `discos` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `discos`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: discos
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autores` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'Aretha Franklin'),(2,'Ray Charles'),(3,'Elvis Presley'),(4,'Sam Cooke'),(5,'John Lennon'),(6,'Kid Cudi'),(7,'Bob Dylan'),(8,'Otis Redding'),(9,'David Guetta'),(10,'James Brown'),(11,'Laura Pausini'),(12,'Andrea Bocelli'),(13,'Il Volo'),(14,'PSY'),(15,'Julio Iglesias'),(16,'Alejandro Sanz'),(17,'Malú'),(18,'David Bisbal');
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds`
--

DROP TABLE IF EXISTS `cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cds` (
  `id_cd` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_comprador` int(11) NOT NULL,
  `anoAdquirido` date NOT NULL,
  `id_discografica` int(11) NOT NULL,
  PRIMARY KEY (`id_cd`),
  KEY `FK_CDS_DISCOGRAFICA_idx` (`id_discografica`),
  KEY `FK_CDS_IDIOMA_idx` (`id_idioma`),
  KEY `FK_CDS_COMPRADORES_idx` (`id_comprador`),
  CONSTRAINT `FK_CDS_COMPRADORES` FOREIGN KEY (`id_comprador`) REFERENCES `compradores` (`id_comprador`),
  CONSTRAINT `FK_CDS_DISCOGRAFICA` FOREIGN KEY (`id_discografica`) REFERENCES `discograficas` (`id_discografica`),
  CONSTRAINT `FK_CDS_IDIOMA` FOREIGN KEY (`id_idioma`) REFERENCES `idioma` (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds`
--

LOCK TABLES `cds` WRITE;
/*!40000 ALTER TABLE `cds` DISABLE KEYS */;
INSERT INTO `cds` VALUES (1,'No-CD-Musica',6,1,'2010-12-15',2),(2,'Music Collection',1,3,'2005-10-10',3),(3,'La Buena Musica',2,3,'2010-09-10',3),(4,'Esto Es Música y Lo Demás Son Tonterías',2,1,'2014-01-05',1),(5,'Musica Forever',5,3,'2019-01-01',5),(6,'This is Music',2,1,'2018-05-05',4),(7,'El Álbum Más Mejor',2,2,'2017-10-12',5),(8,'Nombre Inventado Remix',1,3,'2016-07-02',2),(9,'La Música Es Buena',7,2,'2015-02-07',5),(10,'L\'album musicale',6,2,'2010-01-05',5);
/*!40000 ALTER TABLE `cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds_autores`
--

DROP TABLE IF EXISTS `cds_autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cds_autores` (
  `id_cd` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  PRIMARY KEY (`id_cd`,`id_autor`),
  KEY `FK_CDS_AUTORES_AUTORES_idx` (`id_autor`),
  CONSTRAINT `FK_CDS_AUTORES_AUTORES` FOREIGN KEY (`id_autor`) REFERENCES `autores` (`id_autor`),
  CONSTRAINT `FK_CDS_AUTORES_CDS` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds_autores`
--

LOCK TABLES `cds_autores` WRITE;
/*!40000 ALTER TABLE `cds_autores` DISABLE KEYS */;
INSERT INTO `cds_autores` VALUES (3,1),(2,3),(3,4),(5,5),(1,6),(5,6),(6,7),(6,8),(1,9),(7,9),(8,10),(10,11),(4,12),(10,12),(8,13),(9,14);
/*!40000 ALTER TABLE `cds_autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compradores`
--

DROP TABLE IF EXISTS `compradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `compradores` (
  `id_comprador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_comprador`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compradores`
--

LOCK TABLES `compradores` WRITE;
/*!40000 ALTER TABLE `compradores` DISABLE KEYS */;
INSERT INTO `compradores` VALUES (1,'Daniel Fernández'),(2,'María Ponce'),(3,'Persona Importante');
/*!40000 ALTER TABLE `compradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discograficas`
--

DROP TABLE IF EXISTS `discograficas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `discograficas` (
  `id_discografica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_discografica`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discograficas`
--

LOCK TABLES `discograficas` WRITE;
/*!40000 ALTER TABLE `discograficas` DISABLE KEYS */;
INSERT INTO `discograficas` VALUES (1,'Sony'),(2,'Warner'),(3,'Disc-O-Graphic'),(4,'Ediciones de Canciones'),(5,'Quiebra');
/*!40000 ALTER TABLE `discograficas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos`
--

DROP TABLE IF EXISTS `generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `generos` (
  `id_generos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_generos`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos`
--

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` VALUES (1,'Pop'),(2,'Rock'),(3,'Indie'),(4,'Jazz'),(5,'Clásica'),(6,'Metal'),(7,'Reggae'),(8,'Funky'),(9,'Tecno'),(10,'Infantil');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos_cds`
--

DROP TABLE IF EXISTS `generos_cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `generos_cds` (
  `id_genero` int(11) NOT NULL,
  `id_cd` int(11) NOT NULL,
  PRIMARY KEY (`id_genero`,`id_cd`),
  KEY `FK_GENEROS_CDS_CDS_idx` (`id_cd`),
  CONSTRAINT `FK_GENEROS_CDS_CDS` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id_cd`),
  CONSTRAINT `FK_GENEROS_CDS_GENEROS` FOREIGN KEY (`id_genero`) REFERENCES `generos` (`id_generos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos_cds`
--

LOCK TABLES `generos_cds` WRITE;
/*!40000 ALTER TABLE `generos_cds` DISABLE KEYS */;
INSERT INTO `generos_cds` VALUES (1,1),(2,1),(3,2),(8,2),(10,3),(4,4),(9,4),(7,5),(5,6),(4,7),(2,8),(6,8),(3,9),(5,9),(1,10);
/*!40000 ALTER TABLE `generos_cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idioma`
--

DROP TABLE IF EXISTS `idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `idioma` (
  `id_idioma` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idioma`
--

LOCK TABLES `idioma` WRITE;
/*!40000 ALTER TABLE `idioma` DISABLE KEYS */;
INSERT INTO `idioma` VALUES (1,'Español'),(2,'Inglés'),(3,'Japonés'),(4,'Alemán'),(5,'Portugués'),(6,'Italiano'),(7,'Francés'),(8,'Coreano'),(9,'Chino'),(10,'Ruso');
/*!40000 ALTER TABLE `idioma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-13 11:52:32
