from clases.genero import *
from db.admingenero import *

from clases.discografica import *
from db.admindiscografica import *

from clases.autor import *
from db.adminautor import *

from clases.comprador import *
from db.admincomprador import *

from clases.idioma import *
from db.adminidioma import *

from clases.cd import *
from db.admincd import *

def menuPrincipal():
	return ("""
	------- Administrador BD CDs -------

	1. Administrar Género

	2. Administrar Discográfica

	3. Administrar Autores

	4. Administrar Compradores

	5. Administrar Idiomas

	6. Administrar CDs

	0. Salir

	-------------------------------------
	""")

def menuGenero():
	return ("""
	-------- Administrar Géneros --------

	1. Insertar
	2. Actualizar
	3. Listar
	4. Borrar

	0. Salir

	-------------------------------------
	""")

def menuDiscografica():
	return ("""
	-------- Administrar Discográficas --------

	1. Insertar
	2. Actualizar
	3. Listar
	4. Borrar

	0. Salir

	-------------------------------------
	""")

def menuAutor():
	return ("""
	-------- Administrar Autores --------

	1. Insertar
	2. Actualizar
	3. Listar
	4. Borrar

	0. Salir

	-------------------------------------
	""")

def menuComprador():
	return ("""
	-------- Administrar Compradores --------

	1. Insertar
	2. Actualizar
	3. Listar
	4. Borrar

	0. Salir

	-------------------------------------
	""")

def menuIdioma():
	return ("""
	-------- Administrar Idiomas --------

	1. Insertar
	2. Actualizar
	3. Listar
	4. Borrar

	0. Salir

	-------------------------------------
	""")

def menuCD():
        return ("""
        ---------- Administrar CDs ----------

        1. Insertar
        2. Actualizar
        3. Listar
        4. Borrar

        0. Salir

        -------------------------------------
        """)

def mostar_tabla(admin_lista):
        lista = admin_lista.getALL()
        for elemento in lista:
                print(str(elemento[0]) + ' ' + str(elemento[1]))

opcion = 1
while opcion != "0":

        print(menuPrincipal())

        opcion = input("Por favor, indique una opción: ")


# MENÚ GÉNEROS
        if opcion == "1":

                opcionGenero = 1
                admingenero = AdminGenero('root', 'root', 'localhost', 'discos')

                while opcionGenero != "0":

                        print(menuGenero())

                        opcionGenero = input("Por favor, indique una opción: ")

                        if opcionGenero == "1":
                                nombre = input("Escribe un nuevo genero: ")
                                genero1 = Genero(nombre)
                                admingenero.crear(genero1)

                        elif opcionGenero == "2":
                                mostar_tabla(admingenero)
                                try: 
                                        id_genero = int(input("Qué id quieres editar?: "))
                                        nombre = input("Introduce el nuevo nombre del genero: ")
                                        genero1 = Genero(nombre)
                                        admingenero.actualizar(genero1,id_genero)
                                except ValueError:
                                        print("Opción incorrecta. Debes ingresar un número entero.")
                                
                        elif opcionGenero == "3":
                                mostar_tabla(admingenero)

                              
                        elif opcionGenero == "4":
                                mostar_tabla(admingenero)
                                id_genero = int(input("Qué id quieres eliminar?: "))
                                try:
                                        admingenero.borrar(id_genero)
                                except Exception:
                                        print("No se puede borrar este género porque se está utilizando en otra tabla")
                         
                        elif opcionGenero == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                admingenero.closeConnection()

# MENU DE DISCOGRAFICA

        elif opcion == "2":

                opcionDiscografica = 1
                admindiscografica = AdminDiscografica('root', 'root', 'localhost', 'discos')

                while opcionDiscografica != "0":
                
                        print(menuDiscografica())

                        opcionDiscografica = input("Por favor, indique una opción: ")

                        if opcionDiscografica == "1":
                                nombre = input("Escribe una nueva discografica: ")
                                discografica1 = Discografica(nombre)
                                admindiscografica.crear(discografica1)

                        elif opcionDiscografica == "2":
                                mostar_tabla(admindiscografica)
                                try: 
                                        id_discografica = int(input("Qué id quieres editar?: "))
                                        nombre = input("Introduce el nuevo nombre de la discografica: ")
                                        discografica1 = Discografica(nombre)
                                        admindiscografica.actualizar(discografica1,id_discografica)
                                except ValueError:
                                        print("Opción incorrecta. Debes ingresar un número entero.")
                                
                        elif opcionDiscografica == "3":
                                mostar_tabla(admindiscografica)

                              
                        elif opcionDiscografica == "4":
                                mostar_tabla(admindiscografica)
                                id_discografica = int(input("Qué id quieres eliminar?: "))
                                try:
                                        admindiscografica.borrar(id_discografica)
                                except Exception:
                                        print("No se puede borrar esta discografica porque se está utilizando en otra tabla")
                               
                        elif opcionDiscografica == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                admindiscografica.closeConnection()

# MENU DE AUTOR

        elif opcion == "3":

                opcionAutor = 1
                adminautor = AdminAutor('root', 'root', 'localhost', 'discos')

                while opcionAutor != "0":

                        print(menuAutor())

                        opcionAutor = input("Por favor, indique una opción: ")

                        if opcionAutor == "1":
                                nombre = input("Escribe el nombre del autor: ")
                                autor1 = Autor(nombre)
                                adminautor.crear(autor1)

                        elif opcionAutor == "2":
                                mostar_tabla(adminautor)
                                try:
                                        id_autor = int(input("Qué id quieres editar?: "))
                                        nombre = input("Introduce el nuevo nombre del autor: ")
                                        autor1 = Autor(nombre)
                                        adminautor.actualizar(autor1,id_autor)
                                except ValueError:
                                        print("Opción incorrecta. Debes ingresar un número entero.")
                                
                        elif opcionAutor == "3":
                                mostar_tabla(adminautor)
                              
                        elif opcionAutor == "4":
                                mostar_tabla(adminautor)
                                id_autor = int(input("Qué id quieres eliminar?: "))
                                try:
                                        adminautor.borrar(id_autor)
                                except Exception:
                                        print("No se puede borrar este género porque se está utilizando en otra tabla")
                         
                        elif opcionAutor == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                adminautor.closeConnection()

# MENU DE COMPRADOR

        elif opcion == "4":

                opcionComprador = 1
                admincomprador = AdminComprador('root', 'root', 'localhost', 'discos')

                while opcionComprador != "0":

                        print(menuComprador())

                        opcionComprador = input("Por favor, indique una opción: ")

                        if opcionComprador == "1":
                                nombre = input("Escribe el nombre del comprador: ")
                                comprador1 = Autor(nombre)
                                admincomprador.crear(comprador1)

                        elif opcionComprador == "2":
                                mostar_tabla(admincomprador)
                                try: 
                                        id_comprador = int(input("Qué id quieres editar?: "))
                                        nombre = input("Introduce el nuevo nombre del comprador: ")
                                        comprador1 = Comprador(nombre)
                                        admincomprador.actualizar(comprador1,id_comprador)
                                except ValueError:
                                        print("Opción incorrecta. Debes ingresar un número entero.")
                                
                        elif opcionComprador == "3":
                                mostar_tabla(admincomprador)
                              
                        elif opcionComprador == "4":
                                mostar_tabla(admincomprador)
                                id_comprador = int(input("Qué id quieres eliminar?: "))
                                try:
                                        admincomprador.borrar(id_comprador)
                                except Exception:
                                        print("No se puede borrar este comprador porque se está utilizando en otra tabla")
                         
                        elif opcionComprador == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                admincomprador.closeConnection()

# MENÚ IDIOMAS

        elif opcion == "5":

                opcionIdioma = 1
                adminidioma = AdminIdioma('root', 'root', 'localhost', 'discos')

                while opcionIdioma != "0":
                
                        print(menuIdioma())

                        opcionIdioma = input("Por favor, indique una opción: ")

                        if opcionIdioma == "1":
                                nombre = input("Escribe un nuevo idioma: ")
                                idioma1 = Idioma(nombre)
                                adminidioma.crear(idioma1)

                        elif opcionIdioma == "2":
                                mostar_tabla(adminidioma)
                                try:
                                        id_idioma = int(input("Qué id quieres editar?: "))
                                        nombre = input("Introduce el nuevo idioma: ")
                                        idioma1 = Idioma(nombre)
                                        adminidioma.actualizar(idioma1,id_idioma)
                                except ValueError:
                                        print("Opción incorrecta. Debes ingresar un número entero.")
                                
                                
                        elif opcionIdioma == "3":
                                mostar_tabla(adminidioma)

                              
                        elif opcionIdioma == "4":
                                mostar_tabla(adminidioma)
                                id_idioma = int(input("Qué id quieres eliminar?: "))
                                try:
                                        adminidioma.borrar(id_idioma)
                                except Exception:
                                        print("No se puede borrar este idioma porque se está utilizando en otra tabla")
                               
                        elif opcionIdioma == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                adminidioma.closeConnection()

# MENÚ CDS

        elif opcion == "6":

                opcionCD = 1
                admincd = AdminCD('root', 'root', 'localhost', 'discos')

                while opcionCD != "0":
                
                        print(menuCD())

                        opcionCD = input("Por favor, indique una opción: ")

                        if opcionCD == "1":

                                nombreCD = input("Inserta el nombre de tu nuevo CD: ")
                                fechaCD = input("Inserta la fecha de tu nuevo CD (YYYY-MM-DD): ")
                                nuevoCD = CD(nombreCD,fechaCD)

                                admindiscografica = AdminDiscografica('root', 'root', 'localhost', 'discos')
                                mostar_tabla(admindiscografica)
                                idDiscografica = int(input("¿A qué discográfica corresponde el CD?: "))

                                admingenero = AdminGenero('root', 'root', 'localhost', 'discos')

                                escogerGeneros = "-1"
                                listaGeneros = []

                                while escogerGeneros != "0" or listaGeneros is empty:
                                        mostar_tabla(admingenero)
                                        # PEDIR AL USUARIO QUE INTRODUZCA GENEROS

                                nombre_discografica = admindiscografica.getById(idDiscografica)[0]
                                cdInsertar = CD(nombreCD,fechaCD)
                                cdInsertar.setDiscografica(Discografica(nombre_discografica,idDiscografica))
                                admincd.crear(cdInsertar)
                                admindiscografica.closeConnection()

                        elif opcionCD == "2":
                                mostar_tabla(admincd)
                                id_cd = int(input("¿Qué id quieres modificar?"))
                                cd_original = admincd.getById(id_cd)
                                admindiscografica = AdminDiscografica('root', 'root', 'localhost', 'discos')
                                nombre_discografica = admindiscografica.getById(int(cd_original[0][5]))[0]
                                discografica_original = Discografica(nombre_discografica, int(cd_original[0][5]))
                                cd1 = CD(cd_original[0][1],cd_original[0][4])
                                cd1.setDiscografica(discografica_original)
                                modificar = input("Qué quieres modificar? \n 1: Titulo del CD \n 2: Fecha \n 3: Discografica \n 0: Salir ")
                                if modificar == "1":
                                        titulo_nuevo = input("Escribe el nuevo título del CD: ")
                                        cd1.setNombre(titulo_nuevo)
                                        admincd.actualizar(cd1,id_cd)
                                elif modificar == "2":
                                        fechaCD = input("Inserta la fecha de tu nuevo CD (YYYY-MM-DD): ")
                                        cd1.setFecha(fechaCD)
                                        print(cd1.getFecha())
                                        admincd.actualizar(cd1,id_cd)
                                elif modificar == "3":
                                        mostar_tabla(admindiscografica)
                                        id_nueva_discografica = int(input("Elige a que discografica quieres asignar el CD"))
                                        nombre_discografica = admindiscografica.getById(id_nueva_discografica)[0]
                                        nueva_discografica = Discografica(nombre_discografica,id_nueva_discografica)
                                        cd1.setDiscografica(nueva_discografica)
                                        admincd.actualizar(cd1,id_cd)
                                        
                                elif modificar == "0":
                                        print("Volviendo al menú anterior...")
                                else:
                                        print("Opción incorrecta. Volviendo al menú anterior...")

                                admindiscografica.closeConnection()           

                        elif opcionCD == "3":
                                mostar_tabla(admincd)
                              
                        elif opcionCD == "4":
                                mostar_tabla(admincd)
                                id_cd = int(input("Qué id quieres eliminar?: "))
                                try:
                                        admincd.borrar(id_cd)
                                        print("¡Borrado con éxito!")
                                except Exception:
                                        print("No se puede borrar este CD porque se está utilizando en otra tabla")
                               
                        elif opcionCD == "0":
                                print("Volviendo al menú anterior...")
                        else:
                                print("Opción incorrecta.")

                admincd.closeConnection()

        elif opcion == "0":
                print("Saliendo del programa...")

        else:
                print("Opción incorrecta.")

# Crear CD y asociar una tabla al CD. En CD ponemos ID, nombre, año e id_discografica. Cuales son las discográficas y asignarlo a CD


# cd1.setDiscografica(midiscografica)
# miDiscografica.getId()
